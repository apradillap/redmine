# frozen_string_literal: true

class TwitterWidgetHookListener < Redmine::Hook::ViewListener
  render_on :view_projects_show_left, partial: 'shared/twitter_widget'
end
