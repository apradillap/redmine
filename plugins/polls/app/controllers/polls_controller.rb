# frozen_string_literal: true

class PollsController < ApplicationController
  before_action :find_project, :authorize, only: :index

  def index
    @polls = Poll.all
  end

  def vote
    poll = Poll.find(params[:id])
    poll.vote(params[:answer])
    flash[:notice] = 'Vote saved.' if poll.save
    redirect_to action: 'index'
  end

  private

  def find_project
    # @project variable must be set before calling the authorize filter
    @project = Project.find_by(identifier: params[:project_id])
  end
end
